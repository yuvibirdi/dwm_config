/* Author: YSB */
/* Author_email: yuvibirdi27@gamil.com */
/* This is YSB's DWM config */
/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappih    = 20;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int vertpad            = 10;       /* vertical padding of bar */
static const int sidepad            = 10;       /* horizontal padding of bar */
static const char *fonts[]          = { "JetBrainsMonoNL NF:size=13:antialias:true:weight=Bold" };
static const char dmenufont[]       = "JetBrainsMonoNL NF:size=13:weight=Bold:antialias:true";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

typedef struct {
	const char *name;
	const void *cmd;
} Sp;
const char *spcmd1[] = {"alacritty", "-t", "spterm", "--class", "spterm", NULL };
const char *spcmd2[] = {"alacritty", "-t", "spfm", "--class", "spfm", "-e", "lfub", NULL };
const char *spcmd3[] = {"bitwarden-desktop", NULL };

static Sp scratchpads[] = {
	/* name          cmd  */
	{"spterm",      spcmd1},
	{"spfm",    spcmd2},
	{"bitwarden",   spcmd3},
};

/* tagging */
/* static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }; */
static const char *tags[] = { "","󰨞", "","", "", "󰕼", "", "", };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class           instance    title       tags mask     isfloating   monitor */
	{ NULL,		  "spterm",		NULL,		SPTAG(0),		1,			 -1 },
	{ NULL,		  "spfm",		NULL,		SPTAG(1),		1,			 -1 },
	{ NULL,		  "bitwarden",   	NULL,		SPTAG(2),		1,			 -1 },
	{ "Spotify",       NULL,       NULL,       1 << 5,           0,           -1 },
	{ "discord",       NULL,       NULL,       1 << 6,           0,           -1 },
	{ "slack",       NULL,       NULL,       1 << 7,           0,           0 },
	{ "Slack",       NULL,       NULL,       1 << 7,           0,           0 },
	{ "Emacs",         NULL,       NULL,            1,           0,           -1 },
	{ "jetbrains-clion",         NULL,       NULL,            1 << 1,           0,           -1 },
	{ "Brave-browser", NULL,       NULL,       1 << 3,           0,           -1 },
	{ "GitKraken",     NULL,       NULL,       1 << 2,           0,           -1 },
	{ "firefox",     NULL,       NULL,       1 << 4,           0,           -1},
	{ "rv",            NULL,       NULL,            1,           0,           -1 },
};

#include "XF86keysym.h"
/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */


static const Layout layouts[] = {
	/* symbol     arrange function */
 	{ "[]=",      tile },    /* first entry is default */
 	{ "[M]",      monocle },
	{ "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ "H[]",      deck },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "HHH",      grid },
	{ "###",      nrowgrid },
	{ "---",      horizgrid },
	{ ":::",      gaplessgrid },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "alacritty", NULL };

static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_e,      spawn,           SHCMD("emacsclient -c -a 'emacs'") },
	{0,        XF86XK_AudioRaiseVolume,        spawn,           SHCMD("pamixer --allow-boost -i 5 && kill -44 $(pidof dwmblocks)")},
	{0,        XF86XK_AudioLowerVolume,        spawn,           SHCMD("pamixer --allow-boost -d 5 && kill -44 $(pidof dwmblocks)") },
	{Mod1Mask|ShiftMask,           XK_l,       spawn,           SHCMD("pamixer --allow-boost -i 5 && kill -44 $(pidof dwmblocks)")},
	{Mod1Mask|ShiftMask,           XK_h,       spawn,           SHCMD("pamixer --allow-boost -d 5 && kill -44 $(pidof dwmblocks)") },
	{0,       XF86XK_MonBrightnessDown,        spawn,           SHCMD("light -U 5 && y=$(var=$(light -G) && float=$var && int=${float%.*} && echo $int) && notify-send -t 250 Brightness: $y%") },
	{0,         XF86XK_MonBrightnessUp,        spawn,           SHCMD("light -A 5 && y=$(var=$(light -G) && float=$var && int=${float%.*} && echo $int) && notify-send -t 250 Brightness: $y%") },
	/* Spotify Keybinds */
	{ MODKEY|Mod1Mask, XK_p,    spawn,         SHCMD("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause") },
	{ MODKEY|Mod1Mask, XK_h,    spawn,         SHCMD("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous") },
	{ MODKEY|Mod1Mask, XK_l,    spawn,         SHCMD("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next") },

	{MODKEY, XK_n,                  spawn,         SHCMD("redshift -P -O 4500")},
    {MODKEY|ShiftMask, XK_n,        spawn,         SHCMD("redshift -P -O 6500")},
    {MODKEY|ShiftMask, XK_s,        spawn,         SHCMD("flameshot gui")},
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_h,      setcfact,       {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_l,      setcfact,       {.f = -0.25} },
	{ MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },
	{ MODKEY|ShiftMask,             XK_F3,      zoom,           {0} },
	{ MODKEY,                       XK_u,      incrgaps,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_u,      incrgaps,       {.i = -1 } },
	{ Mod1Mask,                     XK_i,      incrigaps,      {.i = +1 } },
	{ Mod1Mask|ShiftMask,           XK_i,      incrigaps,      {.i = -1 } },
	{ Mod1Mask,                     XK_o,      incrogaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_7,      incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_8,      incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_9,      incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_0,      togglegaps,     {0} },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
	{ MODKEY|Mod1Mask,              XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,       setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_F1,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask,             XK_f,       setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_F2,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_F3,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_F4,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY,                       XK_F5,      setlayout,      {.v = &layouts[6]} },
	{ MODKEY,                       XK_F6,      setlayout,      {.v = &layouts[7]} },
	{ MODKEY,                       XK_F7,      setlayout,      {.v = &layouts[8]} },
	{ MODKEY,                       XK_F8,      setlayout,      {.v = &layouts[9]} },
	{ MODKEY,                       XK_F9,      setlayout,      {.v = &layouts[10]} },
	{ MODKEY,                       XK_F10,     setlayout,      {.v = &layouts[11]} },
	{ MODKEY,                       XK_f,       setlayout,      {.v = &layouts[12]} },
	{ MODKEY,	                 	XK_Tab,    cyclelayout,    {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Tab,    cyclelayout,    {.i = +1 } },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ Mod1Mask,            			XK_a,  	   togglescratch,  {.ui = 0 } },
	{ Mod1Mask,            			XK_s,	   togglescratch,  {.ui = 1 } },
	{ Mod1Mask,            			XK_d,	   togglescratch,  {.ui = 2 } },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_F5,     xrdb,           {.v = NULL } },
	{ MODKEY|ShiftMask,             XK_t,      spawn,          SHCMD("wal -i $(find ~/git/Wallpapers/ -type f \\( -iname '*.jpg' -or -iname '*.png' \\) | shuf -n 1) && xdotool key Super_L+Shift+F5") },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ MODKEY|ControlMask|ShiftMask, XK_q,      spawn,           SHCMD("sudo systemctl restart sddm") },

};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button1,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

