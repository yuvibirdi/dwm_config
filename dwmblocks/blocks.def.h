static const Block blocks[] = {
    /* Icon       Command                        Update Interval   Update Signal */
    {" 🌐 ",      "/opt/dwmblocks/scripts/network",    1,                0},
    {" 🕒 ",      "/opt/dwmblocks/scripts/upt",        60,               0},
    {" 🖥 ",      "/opt/dwmblocks/scripts/memory",     5,                0},
    {" 🔊 ",      "/opt/dwmblocks/scripts/volume",     0,                10},
    {" 🕒 ",      "/opt/dwmblocks/scripts/clock",      1,                0},
    {" ",         "/opt/dwmblocks/scripts/battery",    1,                0},
};
